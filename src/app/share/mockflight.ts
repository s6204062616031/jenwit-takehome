import { Flight } from "../flight";

export class Mockflight {
  public static mflight: Flight[] = [
    {
      fullName : "Jenwit",
      from : "Thailand",
      to : "Singapore",
      type : "One Way",
      adults : 2 ,
      departure : new Date(),
      children : 1,
      infants : 0,
      arrival : new Date(),
    },
    {
      fullName : "humpty",
      from : "Singapore",
      to : "Korea",
      type : "Return",
      adults : 4 ,
      departure : new Date(),
      children : 1,
      infants : 1,
      arrival : new Date(),
    }
  ]
}
